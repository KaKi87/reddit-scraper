const
    PROFILE_USERNAME = Deno.env.get('PROFILE_USERNAME');

import {
    assert,
    assertEquals
} from 'https://deno.land/std@0.88.0/testing/asserts.ts';

import {
    getProfile
} from './mod.js';

Deno.test('getProfile(username)', async () => {
    const res = await getProfile(PROFILE_USERNAME);
    assertEquals(typeof res, 'object');

    const { profile, featuredContentList } = res;

    assertEquals(typeof profile, 'object');
    assertEquals(typeof profile.postKarma, 'number');
    assertEquals(typeof profile.commentKarma, 'number');
    assertEquals(typeof profile.combinedKarma, 'number');
    assertEquals(typeof profile.createdTimestamp, 'number');
    assert(Array.isArray(profile.trophyList));

    assert(Array.isArray(featuredContentList));
    for(const content of featuredContentList){
        assertEquals(typeof content, 'object');
        assertEquals(typeof content.type, 'string');
        assertEquals(typeof content.author, 'string');
        assert(['undefined', 'boolean'].includes(typeof content.isAuthorSubmitter));
        assertEquals(typeof content.subredditName, 'string');
        assertEquals(typeof content.postId, 'string');
        assert(['undefined', 'string'].includes(typeof content.commentId));
        assert(['undefined', 'string'].includes(typeof content.parentCommentId));
        assert(['undefined', 'string'].includes(typeof content.externalLink));
        assertEquals(typeof content.isScoreHidden, 'boolean');
        assert(['undefined', 'number'].includes(typeof content.upvoteCount));
        assert(['undefined', 'number'].includes(typeof content.downvoteCount));
        assert(['undefined', 'number'].includes(typeof content.combinedVoteCount));
        assert(['undefined', 'string'].includes(typeof content.flair));
        assertEquals(typeof content.title, 'string');
        assertEquals(typeof content.createdTimestamp, 'number');
        assertEquals(typeof content.editedTimestamp, 'number');
        assertEquals(typeof content.commentCount, 'number');
        assert(['undefined', 'string'].includes(typeof content.textBody));
        assert(['undefined', 'string'].includes(typeof content.htmlBody));
    }
});
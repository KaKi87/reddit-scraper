import { DOMParser } from 'https://raw.githubusercontent.com/b-fuze/deno-dom/master/deno-dom-wasm.ts';

const
    parseNumberFromText = string => parseInt(string.replace(/[ ,']/g, '')),
    parseDateTextToTimestamp = string => new Date(string).valueOf(),
    baseProfile = doc => {
        const
            postKarma = parseNumberFromText(doc.querySelector('.karma').textContent),
            commentKarma = parseNumberFromText(doc.querySelector('.comment-karma').textContent);
        return {
            postKarma,
            commentKarma,
            combinedKarma: postKarma + commentKarma,
            createdTimestamp: parseDateTextToTimestamp(doc.querySelector('.age time').getAttribute('datetime')),
            trophyList: [...doc.querySelectorAll('.trophy-name')].map(el => el.textContent)
        };
    },
    baseContent = el => {
        if(!el || !el.classList.contains('thing')) return;
        const
            [                 ,,
                subredditName ,,
                postId        ,,
                commentId
            ] = el.getAttribute('data-permalink').split('/'),
            type = el.getAttribute('data-type'),
            isScoreHidden = !!el.querySelector('.score-hidden'),
            bodyElement = el.querySelector('.md');
        return {
            type,
            author: el.getAttribute('data-author'),
            isAuthorSubmitter: type === 'comment' ? !!el.querySelector('.submitter') : undefined,
            subredditName,
            postId,
            commentId: commentId || undefined,
            parentCommentId: baseContent(el.parentElement?.parentElement?.parentElement)?.commentId,
            externalLink: el.querySelector('.outbound')?.getAttribute('data-href-url'),
            isScoreHidden,
            upvoteCount: isScoreHidden ? undefined : parseInt(el.querySelector('.score.likes').getAttribute('title')),
            downvoteCount: isScoreHidden ? undefined : parseInt(el.querySelector('.score.dislikes').getAttribute('title')),
            combinedVoteCount: isScoreHidden ? undefined : parseInt(el.querySelector('.score.unvoted').getAttribute('title')),
            flair: (el.querySelector('.linkflairlabel') || el.querySelector('.flair'))?.textContent,
            title: el.querySelector('a.title')?.textContent,
            createdTimestamp: parseDateTextToTimestamp(el.querySelector('time').getAttribute('datetime')),
            editedTimestamp: parseDateTextToTimestamp(el.querySelector('.edited-timestamp')?.getAttribute('datetime')),
            commentCount: parseInt(el.querySelector('[data-event-action$="comments"]')?.textContent.match(/[0-9]+/)[1]),
            textBody: bodyElement?.textContent,
            htmlBody: bodyElement?.innerHTML
        };
    },
    getProfile = async (
        username,
        {
            sorting = 'new'
        } = {}
    ) => {
        const doc = (new DOMParser).parseFromString(
            await (await fetch(`https://old.reddit.com/u/${username}${sorting !== 'new' ? `?sort=${sorting}` : ''}`)).text(),
            'text/html'
        );
        return {
            profile: baseProfile(doc),
            featuredContentList: [...doc.querySelectorAll('.thing')].map(baseContent)
        };
    },
    getProfilePosts = async (
        username,
        {
            sorting = 'hot'
        } = {}
    ) => {
        const doc = (new DOMParser).parseFromString(
            await (await fetch(`https://old.reddit.com/u/${username}/submitted/${sorting !== 'hot' ? `?sort=${sorting}` : ''}`)).text(),
            'text/html'
        );
        return {
            profile: baseProfile(doc),
            postList: [...doc.querySelectorAll('.thing')].map(baseContent)
        };
    },
    getProfileComments = async (
        username,
        {
            sorting = 'new'
        } = {}
    ) => {
        const doc = (new DOMParser).parseFromString(
            await (await fetch(`https://old.reddit.com/u/${username}/comments/${sorting !== 'new' ? `?sort=${sorting}` : ''}`)).text(),
            'text/html'
        );
        return {
            profile: baseProfile(doc),
            commentList: [...doc.querySelectorAll('.thing')].map(baseContent)
        };
    },
    getPost = async (subredditName, postId) => {
        const
            doc = (new DOMParser).parseFromString(
                await (await fetch(`https://old.reddit.com/r/${subredditName}/comments/${postId}`)).text(),
                'text/html'
            ),
            bodyElement = doc.querySelector(`#siteTable .md`);
        bodyElement?.querySelectorAll('a').forEach(el => el.removeAttribute('rel'));
        return {
            post: baseContent(doc.querySelector('.thing')),
            commentList: [...doc.querySelectorAll('.thing')].slice(1).map(baseContent)
        };
    },
    getSubreddit = async (
        subredditName,
        {
            sorting = 'hot'
        } = {}
    ) => {
        const
            doc = (new DOMParser).parseFromString(
                await (await fetch(`https://old.reddit.com/r/${subredditName}/${sorting !== 'hot' ? `${sorting}/` : ''}`)).text(),
                'text/html'
            ),
            descriptionElement = doc.querySelector('.usertext .md');
        return {
            subreddit: {
                name: doc.querySelector('.redditname').textContent,
                subscriberCount: parseNumberFromText(doc.querySelector('.subscribers .number').textContent),
                onlineUserCount: parseNumberFromText(doc.querySelector('.users-online .number').textContent),
                textDescription: descriptionElement.textContent,
                htmlDescription: descriptionElement.innerHTML,
                featuredModList: [...doc.querySelectorAll('.sidecontentbox li:not([class])')].map(el => ({
                    username: el.querySelector('a').textContent,
                    flair: el.querySelector('.flair')?.getAttribute('title')
                })),
                isModListFull: !new RegExp(/[0-9]/).test(doc.querySelector('.sidecontentbox .more').textContent)
            },
            postList: [...doc.querySelectorAll('.thing')].map(baseContent)
        };
    },
    getSubredditMods = async subredditName => {
        const doc = (new DOMParser).parseFromString(
            await (await fetch(`https://old.reddit.com/r/${subredditName}/about/moderators`)).text(),
            'text/html'
        );
        return {
            modList: [...doc.querySelectorAll('.moderator-table tr')].map(el => ({
                username: el.querySelector('.user a').textContent,
                postKarma: parseNumberFromText(el.querySelector('.user b').textContent),
                joinedTimestamp: parseDateTextToTimestamp(el.querySelector('time').getAttribute('datetime'))
            }))
        }
    },
    search = async (
        query, subredditName,
        {
            sorting = 'relevance'
        } = {}
    ) => {
        const
            doc = (new DOMParser).parseFromString(
                await (await fetch(`https://old.reddit.com${subredditName ? `/r/${subredditName}` : ''}/search?q=${query}${sorting !== 'relevance' ? `&sort=${sorting}` : ''}${subredditName ? '&restrict_sr=on' : ''}`)).text(),
                'text/html'
            ),
            contentListElements = doc.querySelectorAll('.search-result-listing'),
            subredditListElement = subredditName ? undefined : contentListElements[0],
            postListElement = subredditName ? contentListElements[0] : contentListElements[1];
        return {
            subredditList: subredditName ? undefined : [...subredditListElement.querySelectorAll('.search-result')].map(el => {
                const
                    titleElement = el.querySelector('.search-title'),
                    [,,,, name] = titleElement.getAttribute('href').split('/')
                return {
                    name,
                    title: titleElement.textContent,
                    subscriberCount: parseNumberFromText(el.querySelector('.search-subscribers').textContent.slice(0, -9)),
                    shortDescription: el.querySelector('.search-result-body').textContent
                };
            }),
            postList: [...postListElement.querySelectorAll('.search-result')].map(el => {
                const
                    titleElement = el.querySelector('.search-title'),
                    [
                                      ,,,,
                        subredditName ,,
                        postId
                    ] = titleElement.getAttribute('href').split('/'),
                    scoreElement = el.querySelector('.search-score'),
                    bodyElement = el.querySelector('.md');
                return {
                    author: el.querySelector('.author')?.textContent,
                    subredditName,
                    postId,
                    externalLink: el.querySelector('.search-result-footer a')?.getAttribute('href'),
                    isScoreHidden: !scoreElement,
                    combinedVoteCount: parseNumberFromText(scoreElement.textContent.slice(0, -7)),
                    flair: el.querySelector('.linkflairlabel')?.textContent,
                    title: titleElement.textContent,
                    createdTimestamp: parseDateTextToTimestamp(el.querySelector('time').getAttribute('datetime')),
                    commentCount: parseNumberFromText(el.querySelector('.search-comments').textContent.slice(0, -9)),
                    textBody: bodyElement?.textContent,
                    htmlBody: bodyElement?.innerHTML
                }
            })
        };
    };

export {
    getProfile,
    getProfilePosts,
    getProfileComments,
    getPost,
    getSubreddit,
    getSubredditMods,
    search
};